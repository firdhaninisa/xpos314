﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace xpos314.viewmodels
{
    public class VMTblCategory
    {
        public int Id { get; set; }
        //[StringLength(50)]
        //[Unicode(false)]

        
        [Required(ErrorMessage = "Harap isi input, jangan kosong")] // validasi
        [StringLength(10)]
        public string NameCategory { get; set; } = null!;
        //[StringLength(100)]
        //[Unicode(false)]
        public string? Description { get; set; }
        public bool? IsDelete { get; set; }
        public int CreatedBy { get; set; }
        //[Column(TypeName = "datetime")]
        public DateTime CreateDate { get; set; }
        public int? UpdateBy { get; set; }
        //[Column(TypeName = "datetime")]
        public DateTime? UpdateDate { get; set; }
    }
}
