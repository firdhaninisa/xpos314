﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace xpos314.viewmodels
{
    public class VMTblVariant
    {
        public int Id { get; set; }
        public int IdCategory { get; set; }
        //[StringLength(50)]
        //[Unicode(false)]
        public string NameVariant { get; set; } = null!;
        //[StringLength(100)]
        //[Unicode(false)]
        public string? NameCategory { get; set; }
        public string? Description { get; set; }
        public bool? IsDelete { get; set; }
        public int CreateBy { get; set; }
        //[Column(TypeName = "datetime")]
        public DateTime CreateDate { get; set; }
        public int? UpdateBy { get; set; }
        //[Column(TypeName = "datetime")]
        public DateTime? UpdateDate { get; set; }
    }
}
