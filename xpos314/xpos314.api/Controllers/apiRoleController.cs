﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using xpos314.datamodels;
using xpos314.viewmodels;

namespace xpos314.api.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class apiRoleController : ControllerBase
    {
        private readonly XPOS_314Context db;
        private VMResponse respon = new VMResponse();
        private int IdUser = 1;

        public apiRoleController(XPOS_314Context _db)
        {
            this.db = _db;
        }

        [HttpGet("GetAllData")]
        public List<TblRole> GetAllData()
        {
            List<TblRole> data = db.TblRoles.Where(a => a.IsDelete == false).ToList();
            return data;
        }

        [HttpGet("GetDataById/{id}")]
        public TblRole DataById(int id)
        {
            TblRole result = new TblRole();
            result = db.TblRoles.Where(a=>a.Id == id).FirstOrDefault()!;
            return result;
        }

        [HttpPost("Save")]
        public VMResponse Save(TblRole data)
        {
            
            data.IsDelete = false;
            data.CreatedBy = IdUser;
            data.CreatedDate = DateTime.Now;
            data.UpdateBy = IdUser;
            data.UpdateDate = DateTime.Now;

            try
            {
                db.Add(data);
                db.SaveChanges();

                respon.Message = "Data success saved";
            }
            catch (Exception e)
            {
                respon.Success = false;
                respon.Message = "Failed saved : " + e.Message;
            }

            return respon;

        }

        [HttpPut("Edit")]
        public VMResponse Edit(TblRole data)
        {
            TblRole dt = db.TblRoles.Where(a=> a.Id == data.Id).FirstOrDefault()!;

            if(dt != null)
            {
                dt.RoleName = data.RoleName;
                dt.IsDelete = false;
                dt.CreatedBy = IdUser;
                dt.CreatedDate = DateTime.Now;
                dt.UpdateBy = IdUser;
                dt.UpdateDate = DateTime.Now;

                try
                {
                    db.Update(dt);
                    db.SaveChanges();

                    respon.Message = "Data success update";
                }
                catch (Exception e)
                {
                    respon.Success = false;
                    respon.Message = "Update failed : " + e.Message;
                }
            }
            else
            {
                respon.Success = false;
                respon.Message = "Data not found";
            }
            return respon;
        }

        [HttpDelete("Delete/{id}")]
        public VMResponse Delete(int id)
        {
            TblRole dt = db.TblRoles.Where(a => a.Id == id).FirstOrDefault()!;

            if(dt != null)
            {
                dt.IsDelete= true;
                dt.UpdateBy = IdUser;
                dt.UpdateDate = DateTime.Now;

                try
                {
                    db.Update(dt);
                    db.SaveChanges();

                    respon.Message = $"Data {dt.RoleName} success delete";
                }
                catch (Exception e)
                { 
                    respon.Success = false;
                    respon.Message = "Delete failed : " + e.Message;
                }
            }
            else
            {
                respon.Success = false;
                respon.Message = "Data not found";
            }

            return respon;
        }


    }
}
