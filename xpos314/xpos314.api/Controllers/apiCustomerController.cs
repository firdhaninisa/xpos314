﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using xpos314.datamodels;
using xpos314.viewmodels;

namespace xpos314.api.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class apiCustomerController : ControllerBase
    {
        private readonly XPOS_314Context db;
        private VMResponse respon = new VMResponse();
        private int IdUser = 1;

        public apiCustomerController(XPOS_314Context _db)
        {
            this.db = _db;
        }

        [HttpGet("GetAllData")]
        public List<VMTblCustomer> GetAllData()
        {
            List<VMTblCustomer> data = (from cus in db.TblCustomers
                                       join rol in db.TblRoles on cus.IdRole equals rol.Id
                                       where cus.IsDelete == false
                                       select new VMTblCustomer
                                       {
                                           Id = cus.Id,
                                           NameCustomer = cus.NameCustomer,
                                           Email = cus.Email,

                                           IdRole = cus.IdRole,
                                           RoleName = rol.RoleName


                                       }).ToList();
            return data;
        }

        [HttpGet("GetDataById/{id}")]
        public VMTblCustomer GetDataById(int id)
        {
            VMTblCustomer data = (from cus in db.TblCustomers
                                  join rol in db.TblRoles on cus.IdRole equals rol.Id
                                  where cus.IsDelete == false
                                  select new VMTblCustomer
                                  {
                                      Id = cus.Id,
                                      NameCustomer = cus.NameCustomer,
                                      Email = cus.Email,

                                      IdRole = cus.IdRole,
                                      RoleName = rol.RoleName

                                  }).FirstOrDefault()!;
            return data;
        }

        [HttpPost("Save")]
        public VMResponse Save(TblCustomer data)
        {
            data.CreateBy = IdUser;
            data.CreateDate = DateTime.Now;
            data.IsDelete = false;

            try
            {
                db.Add(data);
                db.SaveChanges();

                respon.Message = "Data success saved";
            }
            catch (Exception e)
            {
                respon.Success = false;
                respon.Message = "Failed saved : " + e.Message;
            }

            return respon;
        }

        [HttpPut("Edit")]
        public VMResponse Edit(TblCustomer data)
        {
            TblCustomer dt = db.TblCustomers.Where(a => a.Id == data.Id).FirstOrDefault()!;

            if (dt != null)
            {
                dt.NameCustomer = data.NameCustomer;
                dt.NameCustomer = data.NameCustomer;
                dt.IdRole = data.IdRole;
                dt.CreateBy = IdUser;
                dt.UpdateDate = DateTime.Now;

                try
                {
                    db.Update(dt);
                    db.SaveChanges();

                    respon.Message = "Data success update";

                }
                catch (Exception e)
                {
                    respon.Success = false;
                    respon.Message = "Updated failed :" + e.Message;
                }
            }
            else
            {
                respon.Success = false;
                respon.Message = "Data not found";
            }

            return respon;
        }

        [HttpDelete("Delete/{id}")]

        public VMResponse Delete(int id)
        {
            TblCustomer dt = db.TblCustomers.Where(a => a.Id == id).FirstOrDefault()!;

            if (dt != null)
            {
                dt.IsDelete = true;
                dt.UpdateBy = IdUser;
                dt.UpdateDate = DateTime.Now;

                try
                {
                    db.Update(dt);
                    db.SaveChanges();

                    respon.Message = $"Data {dt.NameCustomer} success delete";
                }
                catch (Exception e)
                {
                    respon.Success = false;
                    respon.Message = "Delete failed : " + e.Message;
                }
            }
            else
            {
                respon.Success = false;
                respon.Message = "Data not found";
            }

            return respon;
        }
    }
}
