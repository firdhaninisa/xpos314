﻿using AutoMapper;
using xpos314.datamodels;
using xpos314.viewmodels;

namespace xpos314.web.Services
{
    public class VariantTryService
    {
        private readonly XPOS_314Context db;
        VMResponse respon = new VMResponse();
        int IdUser = 1;

        public VariantTryService(XPOS_314Context _db)
        {
            this.db = _db;
        }

        public static IMapper GetMapper()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<TblVariant, VMTblVariant>();
                cfg.CreateMap<VMTblVariant, TblVariant>();
            });

            IMapper mapper = config.CreateMapper();
            return mapper;
        }

        public List<VMTblVariant> GetAllData()
        {
            List<VMTblVariant> dataView = (from v in db.TblVariants
                                           join c in db.TblCategories on v.IdCategory equals c.Id
                                           where v.IsDelete == false
                                           select new VMTblVariant
                                           {
                                               Id = v.Id,
                                               NameVariant = v.NameVariant,
                                               Description = v.Description,

                                               IdCategory = v.IdCategory,
                                               NameCategory = c.NameCategory
                                           }
                                           ).ToList();
            return dataView;
        }

        //coba buat submit

        public VMResponse Create(VMTblVariant dataView)
        {
            //TblVariant dataModel = new TblVariant();            
            TblVariant dataModel = GetMapper().Map<TblVariant>(dataView);
            dataModel.CreateBy = IdUser;
            dataModel.CreateDate = DateTime.Now; //1
            dataModel.IsDelete = false; //2

            try //4
            {
                db.Add(dataModel); //6
                db.SaveChanges();

                respon.Message = "Data success saved"; //7
                respon.Entity = dataModel; //8

            }
            catch (Exception e)//5
            {
                respon.Success = false; //9
                respon.Message = "Failed saved : " + e.Message; //10
                respon.Entity = dataView;
            }

            return respon; //3
        }

        public VMTblVariant GetById(int id)
        {
            VMTblVariant dataView = (from v in db.TblVariants
                                     join c in db.TblCategories on v.IdCategory equals c.Id
                                     where v.IsDelete == false && v.Id == id
                                     select new VMTblVariant
                                     {
                                         Id = v.Id,
                                         NameVariant = v.NameVariant,
                                         Description = v.Description,
                                         CreateBy = v.CreateBy,
                                         CreateDate = v.CreateDate,
                                         UpdateBy = v.UpdateBy,
                                         UpdateDate = v.UpdateDate,
                                         IdCategory = v.IdCategory,
                                         NameCategory = c.NameCategory
                                     }
                                    ).FirstOrDefault()!;
            return dataView;
        }

        public VMResponse Edit(VMTblVariant dataView)
        {
            TblVariant dataModel = db.TblVariants.Find(dataView.Id)!;

            dataModel.NameVariant = dataView.NameVariant;
            dataModel.IdCategory = dataView.IdCategory;
            dataModel.Description = dataView.Description;
            dataModel.UpdateBy = IdUser;
            dataModel.UpdateDate = DateTime.Now;

            try
            {
                db.Update(dataModel);
                db.SaveChanges();

                respon.Message = "Data success saved";
                respon.Entity = GetMapper().Map<VMTblVariant>(dataModel);
            }
            catch(Exception e)
            {
                respon.Success = false;
                respon.Message = "Failed saved : " + e.Message;
                respon.Entity = GetMapper().Map<VMTblVariant>(dataModel);

            }

            return respon;
        }

        public VMResponse Detail(VMTblVariant dataView)
        {

            TblVariant dataModel = db.TblVariants.Find(dataView.Id)!;
            dataModel.NameVariant = dataView.NameVariant;
            dataModel.IdCategory = dataView.IdCategory;
            dataModel.Description = dataView.Description;
            dataModel.UpdateBy = IdUser;
            dataModel.UpdateDate = DateTime.Now;

            return respon;
        }

        public VMResponse Delete(VMTblVariant dataView)
        {
            TblVariant dataModel = db.TblVariants.Find(dataView.Id)!;
            dataModel.IsDelete = true;
            dataModel.UpdateBy = IdUser;
            dataModel.UpdateDate = DateTime.Now;

            try
            {
                db.Update(dataModel);
                db.SaveChanges();

                respon.Message = "Failed saved";
                respon.Entity = GetMapper().Map<VMTblVariant>(dataModel);


            }
            catch(Exception e)
            {
                respon.Success = false;
                respon.Message = "Failed saved : " + e.Message;
                respon.Entity = GetMapper().Map<VMTblVariant>(dataModel);
            }

            return respon;
        }
    }
}
