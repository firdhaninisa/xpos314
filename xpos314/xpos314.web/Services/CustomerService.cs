﻿using Newtonsoft.Json;
using System.Text;
using xpos314.viewmodels;

namespace xpos314.web.Services
{
    public class CustomerService
    {
        private static readonly HttpClient client = new HttpClient();
        private IConfiguration configuration;
        private string RouteAPI = "";
        private VMResponse respon = new VMResponse();

        public CustomerService(IConfiguration _configuration)
        {
            this.configuration = _configuration;
            this.RouteAPI = this.configuration["RouteAPI"];
        }

        public async Task<List<VMTblCustomer>> GetAllData()
        {
            List<VMTblCustomer> data = new List<VMTblCustomer>();

            string apiResponse = await client.GetStringAsync(RouteAPI + "apiCustomer/GetAllData"); //client nama bisa apa aja
            data = JsonConvert.DeserializeObject<List<VMTblCustomer>>(apiResponse)!;

            return data;
        }

        public async Task<VMResponse> Create(VMTblCustomer dataParam)
        {
            //proses convert dari object ke string
            string json = JsonConvert.SerializeObject(dataParam);

            //proses mengubah string menjadi json lalu dikirim sebagai request body
            StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");

            //proses memanggil API dan mengirimkan body
            var request = await client.PostAsync(RouteAPI + "apiCustomer/Save", content);

            if (request.IsSuccessStatusCode)
            {
                //proses membaca respon dari API
                var apiRespon = await request.Content.ReadAsStringAsync();

                //proses convert hasil respon dari API ke object
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon)!;
            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }

            return respon;
        }

        public async Task<VMTblCustomer> GetDataById(int id)
        {
            VMTblCustomer data = new VMTblCustomer();
            string apiResponse = await client.GetStringAsync(RouteAPI + $"apiCustomer/GetDataById/{id}"); //client nama bisa apa aja
            data = JsonConvert.DeserializeObject<VMTblCustomer>(apiResponse)!;

            return data;
        }

        public async Task<List<VMTblCustomer>> GetDataByIdRole(int id)
        {
            List<VMTblCustomer> data = new List<VMTblCustomer>();
            string apiResponse = await client.GetStringAsync(RouteAPI + $"apiCustomer/GetDataByIdRole/{id}"); //client nama bisa apa aja
            data = JsonConvert.DeserializeObject<List<VMTblCustomer>>(apiResponse)!;

            return data;
        }
    }
}
