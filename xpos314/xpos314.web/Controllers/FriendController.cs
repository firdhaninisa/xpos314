﻿using Microsoft.AspNetCore.Mvc;
using System.Security.Cryptography.X509Certificates;
using xpos314.web.Models;

namespace xpos314.web.Controllers
{
    public class FriendController : Controller
    {
        
        private static List<Friend> teman = new List<Friend>()
        {
            new Friend() { Id = 1, Name = "Anwar", Address = "Ragunan"},
            new Friend() { Id = 2, Name = "Asti", Address = "Garut"},
            //new Friend() { Id = 3, Name = "Isni", Address = "Cimahi"}
        };

        public IActionResult Index()
        {

            ViewBag.listFriend = teman;
            return View(teman);

            //ViewBag.listFriend = teman;
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Create(Friend friend)
        {
            teman.Add(friend);
            return RedirectToAction("Index");
        }

        public IActionResult Edit(int id)
        {
            Friend friend = teman.Find(a => a.Id == id)!;
            return View(friend);
        }

        [HttpPost]
        public IActionResult Edit(Friend data)
        {
            Friend friend = teman.Find(a => a.Id == data.Id)!;

            int index = teman.IndexOf(friend);

            if(index > -1)
            {
                teman[index].Id = data.Id;
                teman[index].Name = data.Name;
                teman[index].Address = data.Address;
            }

            return RedirectToAction("Index");
        }

        public IActionResult Detail(int id)
        {
            Friend friend = teman.Find(a => a.Id == id)!;
            return View(friend);
        }

        /*public IActionResult Delete(int id)
        {
            Friend friend = teman.Find(a => a.Id == id)!;
            return View(friend);
        }

        [HttpPost]
        public IActionResult Delete(string id)
        {
            Friend data = teman.Find(a => a.Id == int.Parse(id))!;
            teman.Remove(data);
            return RedirectToAction("Index");
        }*/

        [HttpGet]
        [HttpPost]
        public IActionResult delete(int id)
        {
            Friend friend = teman.Find(a => a.Id == id)!;
            if(HttpContext.Request.Method == "POST")
            {
                teman.Remove(friend);
                return RedirectToAction("Index");
            }
            else
            {
                return View(friend);
            }
        }
    }
}
