﻿using Microsoft.AspNetCore.Mvc;
using xpos314.datamodels;
using xpos314.viewmodels;
using xpos314.web.Services;

namespace xpos314.web.Controllers
{
    public class CategoryController : Controller
    {
        private CategoryService categoryService;
        //private IConfiguration configuration;
        private int IdUser = 1;
        
        //public CategoryController(IConfiguration _configuration)
        //public CategoryController(IConfiguration _configuration)
        //{
        //    this.configuration = _configuration;
        //    this.categoryService = new CategoryService(this.configuration); //konstraktornya harus diisi dengan parameter
        //}

        public CategoryController(CategoryService _categoryService)
        {
            //this.configuration = _configuration;
            //this.categoryService = new CategoryService(this.configuration); //konstraktornya harus diisi dengan parameter
            //this.variantService = new VariantService(this.configuration);
            this.categoryService = _categoryService;

        }

        public async Task<IActionResult> Index(string sortOrder, 
                                                string searchString, 
                                                string currentFilter, 
                                                int? pageNumber, 
                                                int? pageSize)
        
        
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.CurrentPageSize = pageSize;
            ViewBag.NameSort = string.IsNullOrEmpty(sortOrder) ? "name_desc" : "";

            if (searchString != null)
            {
                pageNumber = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;

            List<TblCategory> data = await categoryService.GetAllData();

            if (!string.IsNullOrEmpty(searchString))
            {
                data = data.Where(a=>a.NameCategory.ToLower().Contains(searchString.ToLower()) 
                    || a.Description.ToLower().Contains(searchString.ToLower())                   
                    ).ToList();
            }

            switch (sortOrder)
            {
                case "name_desc":
                    data = data.OrderByDescending(a => a.NameCategory).ToList();
                    break;
                default:
                    data = data.OrderBy(a => a.NameCategory).ToList();
                    break;
            }

            return View(PaginatedList<TblCategory>.CreateAsync(data, pageNumber ??1, pageSize??3));
        }

        public IActionResult Create()
        {
            TblCategory data = new TblCategory();
            return PartialView(data);
        }

        [HttpPost]
        public async Task<IActionResult> Create(TblCategory dataParam)
        {
            dataParam.CreatedBy = IdUser;
            VMResponse respon = await categoryService.Create(dataParam);

            if (respon.Success)
            {
                return Json(new {dataRespon = respon});
            }

            return View(dataParam);
        }

        public async Task<JsonResult> CheckNameIsExist(string nameCategory)
        {
            bool isExist = await categoryService.CheckCategoryByName(nameCategory);
            return Json(isExist);
        }

        public async Task<IActionResult> Edit(int id)
        {
            TblCategory data = await categoryService.GetDataById(id);
            return PartialView(data);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(TblCategory dataParam)
        {
            dataParam.UpdateBy = IdUser;
            VMResponse respon = await categoryService.Edit(dataParam);

            if(respon.Success)
            {
                return Json(new { dataRespon = respon });
            }

            return View(dataParam);
        }

        public async Task<IActionResult> Delete(int id)
        {
            TblCategory data = await categoryService.GetDataById(id);
            return PartialView(data);
        }

        [HttpPost]
        public async Task<IActionResult> SureDelete(int id)
        {
            int createBy = IdUser;
            VMResponse respon = await categoryService.Delete(id, createBy);

            if(respon.Success)
            {
                return RedirectToAction("Index");
            }

            return RedirectToAction("Index", id);
        }

        //coba sendiri

        public async Task<IActionResult> Detail(int id)
        {
            TblCategory data = await categoryService.GetDataById(id);
            return PartialView(data);
        }
    }
}
