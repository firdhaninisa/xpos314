﻿using Microsoft.AspNetCore.Mvc;
using xpos314.datamodels;
using xpos314.viewmodels;
using xpos314.web.Services;

namespace xpos314.web.Controllers
{
    public class VariantTryController : Controller
    {

        private readonly XPOS_314Context db;
        private readonly VariantTryService variantTryService;
        private readonly CategoryTryService categoryTryService;

        private static VMPage page = new VMPage();

        public VariantTryController(XPOS_314Context _db)
        {
            this.db = _db;
            this.variantTryService = new VariantTryService(db);
            this.categoryTryService = new CategoryTryService(db);
        }

        public IActionResult Index(VMPage pg)
        {
            ViewBag.idSort = string.IsNullOrEmpty(pg.sortOrder) ? "id_desc" : "";
            ViewBag.nameSort = pg.sortOrder == "name" ? "name_desc" : "name";
            ViewBag.currentSort = pg.sortOrder;
            ViewBag.currentShowData = pg.showData;

            if(pg.searchString != null)
            {
                pg.pageNumber = 1;
            }
            else
            {
                pg.searchString = pg.currentFilter;
            }

            ViewBag.currentFilter = pg.searchString;

            List<VMTblVariant> dataView = variantTryService.GetAllData();

            if (!string.IsNullOrEmpty(pg.searchString))
            {
                dataView = dataView.Where(a => a.NameVariant.ToLower().Contains(pg.searchString.ToLower())
                || a.NameCategory.ToLower().Contains(pg.searchString.ToLower())).ToList();                               
            }

            switch (pg.sortOrder)
            {
                case "name_desc":
                    dataView = dataView.OrderByDescending(a => a.NameVariant).ToList();
                    break;
                case "name":
                    dataView = dataView.OrderBy(a => a.NameVariant).ToList();
                    break;
                case "id_desc":
                    dataView = dataView.OrderByDescending(a => a.Id).ToList();
                    break;
                default:
                    dataView = dataView.OrderBy(a => a.Id).ToList();
                    break;
            }

            int pageSize = pg.showData ?? 3;

            page = pg;

            return View(PaginatedList<VMTblVariant>.CreateAsync(dataView, pg.pageNumber ?? 1, pageSize));
        }


        public IActionResult Create()
        {
            VMTblVariant dataView = new VMTblVariant();
            ViewBag.DropdownCategory = categoryTryService.GetAllData();
            return View(dataView);
        }

        //coba submit

        [HttpPost]
        public IActionResult Create(VMTblVariant dataView)
        {
            VMResponse respon = new VMResponse();

            if (ModelState.IsValid)
            {
                respon = variantTryService.Create(dataView);

                if (respon.Success)
                {
                    return RedirectToAction("Index", page);
                }
            }

            ViewBag.DropdownCategory = categoryTryService.GetAllData();
            respon.Entity = dataView;
            return View(respon.Entity);
        }

        public IActionResult Edit(int id)
        {
            VMTblVariant dataView = variantTryService.GetById(id);
            ViewBag.DropdownCategory = categoryTryService.GetAllData();
            return View(dataView);
        }

        [HttpPost]
        public IActionResult Edit(VMTblVariant dataView)
        {
            VMResponse respon = new VMResponse();

            if (ModelState.IsValid)
            {
                respon = variantTryService.Edit(dataView);

                if (respon.Success)
                {
                    return RedirectToAction("Index", page);
                }
            }

            ViewBag.DropdownCategory = categoryTryService.GetAllData();
            respon.Entity = dataView;
            return View(respon.Entity);
        }

        public IActionResult Detail (int id)
        {
            VMTblVariant dataView = variantTryService.GetById(id);
            ViewBag.DropdownCategory = categoryTryService.GetAllData();
            return View(dataView);
        }

        public IActionResult Delete(int id)
        {
            VMTblVariant dataView = variantTryService.GetById(id);            
            return View(dataView);
        }

        [HttpPost]
        public IActionResult Delete(VMTblVariant dataView)
        {
            VMResponse respon = variantTryService.Delete(dataView);

            if (respon.Success)
            {
                return RedirectToAction("Index", page);
            }
            return View(respon.Entity);
        }


    }
}
